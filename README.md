## Description

Flutter 3.0 Chat App, with Node Js, Mongoose and Token JWT Authentication.

The app allows you to have private and room chats.

# Flutter/Node/MongoDB Chat

This folder has the files for the Flutter App.

You will need the folder with the [Node Js](https://github.com/ecorbero/_rodrileonel_ChatNode) files as well.

## Screenshots

Login Page (Android):

![Chat Login Page](https://raw.githubusercontent.com/ecorbero/_rodrileonel_ChatFlutter/master/screenshots/login.PNG "Chat Login Page")

Register Page (Android):

![Chat Register Page](https://raw.githubusercontent.com/ecorbero/_rodrileonel_ChatFlutter/master/screenshots/register.PNG "Chat Register Page")

Users Page (Web App):

![Chat Users Page](https://raw.githubusercontent.com/ecorbero/_rodrileonel_ChatFlutter/master/screenshots/users.PNG "Chat Users Page")

Private Chat Page (Web App):

![Chat chat Page](https://raw.githubusercontent.com/ecorbero/_rodrileonel_ChatFlutter/master/screenshots/chat.PNG "Chat Chat Page")

Room Chat Page (Web App):

![Chat Room Page](https://raw.githubusercontent.com/ecorbero/_rodrileonel_ChatFlutter/master/screenshots/group.PNG "Chat Room Page")

## Getting Started

To start this project: 

0. Clone this Repository
1. Clone the [Node Files](https://github.com/ecorbero/_rodrileonel_ChatNode), run de Node app, wait for the message "DB Online"
2. Run on your terminal => flutter pub get
3. Select your Android Emulator or a Browser, and run the program
4. The app has yet to be tested for iOS, let me know if it works!

## Online Demo

https://ecorbero.github.io/flatter_chat

If you don't want to register, you can use one of these demo users: 1@gmail.com, 2@gmail.com, 3@gmail.com, 4@gmail.com. Password = 1234

If you want to try the room chat, you can open different private tabs in your browser and login with several users at the same time.

## Bibliography

https://github.com/rodrileonel/ChatSocket