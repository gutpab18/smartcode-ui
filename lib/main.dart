import 'package:smartcode_ui/pages/loading_page.dart';
import 'package:smartcode_ui/routes/routes.dart';
import 'package:smartcode_ui/services/auth_service.dart';
import 'package:smartcode_ui/services/chat_service.dart';
import 'package:smartcode_ui/services/room_service.dart';
import 'package:smartcode_ui/services/socket.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialization(null);
  FlutterNativeSplash.remove();
  runApp(MyApp());
}

Future initialization(BuildContext? context) async {
  //  Load Resources
  await Future.delayed(Duration(seconds: 3));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AuthService(),
        ),
        ChangeNotifierProvider(
          create: (_) => SocketService(),
        ),
        ChangeNotifierProvider(
          create: (_) => ChatService(),
        ),
        ChangeNotifierProvider(
          create: (_) => RoomService(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: const Color(0xFF6E5172),
          focusColor: const Color(0xFF34B0AD),
          fontFamily: 'Poppins',
          scaffoldBackgroundColor: const Color(0xFFFFFFFF),
          buttonTheme: ButtonThemeData( // 4
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0.0)),
            buttonColor: const Color(0xFFEF426F)
          )
        ),
        debugShowCheckedModeBanner: false,
        title: 'SmartCode App',
        initialRoute: LoadingPage.routeName,
        routes: routes,
      ),
    );
  }
}
