// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    required this.username,
    required this.id,
    required this.role,
    this.online = true
  });

  String username;
  String id;
  String role;
  bool online;

  factory User.fromJson(Map<String, dynamic> json) => User(
        username: json["username"],
        id: json["_id"],
        role: json["role"],
      );

  Map<String, dynamic> toJson() => {
        "username": username,
        "id": id,
        "role": role,
      };
}
