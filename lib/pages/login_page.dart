import 'package:smartcode_ui/helpers/show_alert.dart';
import 'package:smartcode_ui/pages/register_page.dart';
import 'package:smartcode_ui/pages/users_page.dart';
import 'package:smartcode_ui/services/auth_service.dart';
import 'package:smartcode_ui/services/socket.dart';
import 'package:smartcode_ui/widgets/input.dart';
import 'package:smartcode_ui/widgets/login_register_button.dart';
import 'package:smartcode_ui/widgets/logo.dart';
import 'package:smartcode_ui/widgets/button_sign.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  static const routeName = 'Login';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: Colors.grey[300],
        body: SafeArea(
      child: SingleChildScrollView(
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const SizedBox(
                  height: 5,
                ),
                Logo(),
                const SizedBox(
                  height: 5,
                ),
                _Form(),
                const SizedBox(
                  height: 5,
                ),
                const LoginRegisterButton(
                  routeName: RegisterPage.routeName,
                  label: "First Time Here?",
                  textButton: 'Create an Account!',
                ),
                const SizedBox(height: 15),
                const Text('Terms and Conditions '),
              ]),
        ),
      ),
    ));
  }
}

class _Form extends StatelessWidget {
  final usernameController = TextEditingController();
  final passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketService>(context);
    return Center(
      child: Container(
        constraints: const BoxConstraints(minWidth: 180, maxWidth: 600),
        padding: const EdgeInsets.symmetric(horizontal: 40),
        child: Column(children: [
          Input(
            icon: Icons.email_outlined,
            placeholder: 'Username',
            maxLength: 32,
            controller: usernameController,
          ),
          const SizedBox(
            height: 16,
          ),
          Input(
            icon: Icons.lock_outlined,
            placeholder: 'Password',
            maxLength: 32,
            controller: passController,
            hidden: true,
          ),
          const SizedBox(
            height: 16,
          ),
          SignButton(
            label: 'Log In',
            press: authService.logeando
                ? null
                : () async {
                    FocusScope.of(context).unfocus();
                    final loginOK = await authService.login(
                        usernameController.text.trim(), passController.text);
                    if (loginOK) {
                      //conectar a socketserver
                      socketService.connect("noroom");
                      //navegar a la pantalla de usuarios
                      Navigator.pushReplacementNamed(
                          context, UsersPage.routeName);
                    } else {
                      showAlert(context, 'Login Incorrect',
                          'Check your credentials and try again');
                    }
                  },
          ),
        ]),
      ),
    );
  }
}
