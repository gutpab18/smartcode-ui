import 'package:flutter/material.dart';

import 'package:smartcode_ui/pages/chat_page.dart';
import 'package:smartcode_ui/pages/loading_page.dart';
import 'package:smartcode_ui/pages/login_page.dart';
import 'package:smartcode_ui/pages/newroom_page.dart';
import 'package:smartcode_ui/pages/register_page.dart';
import 'package:smartcode_ui/pages/users_page.dart';

final Map<String, Widget Function(BuildContext)> routes = {
  UsersPage.routeName: (_) => UsersPage(),
  ChatPage.routeName: (_) => ChatPage(),
  LoginPage.routeName: (_) => LoginPage(),
  RegisterPage.routeName: (_) => RegisterPage(),
  LoadingPage.routeName: (_) => LoadingPage(),
  NewroomPage.routeName: (_) => NewroomPage(),
};
