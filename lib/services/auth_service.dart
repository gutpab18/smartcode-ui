import 'dart:convert';

import 'package:smartcode_ui/global/environment.dart';
import 'package:smartcode_ui/models/login_response.dart';
import 'package:smartcode_ui/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
//import 'package:flutter/foundation.dart' show kIsWeb;

class AuthService with ChangeNotifier {
  late User user;
  bool _logeando = false;

  final _storage = const FlutterSecureStorage();

  bool get logeando => _logeando;
  set logeando(value) {
    _logeando = value;
    notifyListeners();
  }

  Future login(String username, String password) async {
    logeando = true;

    final request = {'username': username, 'password': password};

    final response = await http.post(Uri.parse('${Environment.apiUrl}/auth/login'),
        body: jsonEncode(request),
        headers: {'Content-Type': 'application/json'});

    //print(response.body);

    logeando = false;

    if (response.statusCode == 200) {
      final data = loginResponseFromJson(response.body);
      user = data.user;
      await _saveToken(data.accessToken, data.refreshToken);
      return true;
    } else {
      return false;
    }
  }

  Future register(String name, String username, String password) async {
    logeando = true;

    final request = {'name': name, 'username': username, 'password': password};

    final response = await http.post(
        Uri.parse('${Environment.apiUrl}/login/new'),
        body: jsonEncode(request),
        headers: {'Content-Type': 'application/json'});
    logeando = false;

    if (response.statusCode == 200) {
      final data = loginResponseFromJson(response.body);
      user = data.user;
      await _saveToken(data.accessToken, data.refreshToken);
      return true;
    } else {
      return jsonDecode(response.body)['msg'];
    }
  }

  Future _saveToken(String accessToken, String refreshToken) async {
    await _storage.write(key: 'accessToken', value: accessToken);
    await _storage.write(key: 'refreshToken', value: refreshToken);
    var readAccessToken = await _storage.read(key: 'accessToken');
    var readRefreshToken = await _storage.read(key: 'refreshToken');
    print(readAccessToken);
    print(readRefreshToken);
    return {
      readAccessToken,
      readRefreshToken
    };
  }

  Future<bool> logged() async {
    //final token = await _storage.read(key: 'token');

    //print("e22e");

    //String url = "http://localhost:3000/api/login";

    //final response = await http.get(Uri.parse(url));
    //final response = await http.get(Uri.parse('localhost:3000/api/login/renew'),
    //    headers: {'Content-Type': 'application/json', 'x-token': token!});

    //print(response);
/*
    String _baseUrl = '192.168.1.133:3000';
    String _path = 'api/login';
    Uri uri = Uri.http(_baseUrl, _path);
    var client = http.Client();

    print("eeee");
    //http.Response response = await http.get(uri);
    var response = await client.post(uri,
        headers: {'Content-Type': 'application/json', 'x-token': token!}
        //headers: requestHeaders,
        //body: jsonEncode(model.toJson()),
        );
*/
/*
    // If built for Web Skip Check
    if (kIsWeb) {
      logout();
      return false;
    }
*/
    var accessToken = await _storage.read(key: 'accessToken');
    var refreshToken = await _storage.read(key: 'refreshToken');

    accessToken ??= "na";
    refreshToken ??= "na";

    //print("dfffeef");
    //print(token);

    //final response = await http.get(
    //    Uri.parse('${Environment.apiUrl}/login/renew'),
    //    headers: {'Content-Type': 'application/json', 'x-token': token!});

    //print(Environment.apiUrl);

    final response = await http.post(
        Uri.parse('${Environment.apiUrl}/auth/refresh'),
        headers: {
          'Content-Type': 'application/json',
          'x-access-token': accessToken,
          'x-refresh-token': refreshToken
        }
    );

    //print("sssq");

    //print(response.body);

    if (response.statusCode == 200) {
      final data = loginResponseFromJson(response.body);
      user = data.user;
      await _saveToken(data.accessToken, data.refreshToken);
      return true;
    } else {
      logout();
      return false;
    }
  }

  Future logout() async {
    await _storage.delete(key: 'accessToken');
  }

  //si quiero el token sin referenciar a la clase, la hago static
  static Future<String?> getToken() async {
    final _storage = FlutterSecureStorage();
    return await _storage.read(key: 'accessToken');
  }

  //si quiero el token sin referenciar a la clase, la hago static
  static Future<void> deleteToken() async {
    final _storage = FlutterSecureStorage();
    await _storage.delete(key: 'accessToken');
    await _storage.delete(key: 'refreshToken');
  }
}
